package com.company;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by christian on 17/12/15.
 */
public class Exercices {

    /**
     * Calcula el factorial de un número N
     * @param n
     * @return
     */
    public int factorial(int n) {
        if ( n < 0 ) {
            return 0;
        }

        return IntStream.rangeClosed(1, n)
                .reduce(1, (a, b) -> a * b);
    }

    /**
     * Imprime la secuencia de números n^2 de 1 hasta n
     * @param n
     */
    public void printSequenceN2(int n) {
        IntStream.rangeClosed(1, n)
                .map(x -> x * x)
                .forEach(x -> System.out.print(x + " "));
    }

    /**
     * Imprime la secuencia inclusiva de numeros de min hasta max
     * @param min
     * @param max
     */
    public void printSequence(int min, int max) {
        IntStream.rangeClosed(min, max)
                .forEach(System.out::println);
    }

    /**
     * Regresa true si el número es primo
     * @param N
     * @return
     */
    public boolean isPrime(int N) {
        if ( N <= 0 ) {
            return false;
        }

        return IntStream.rangeClosed(1, N)
                .filter( x -> N%x == 0 )
                .count() == 2;
    }

    /**
     * Imprime todos los números primos que se encuentren en el arreglo valores
     * @param valores
     */
    public void printPrimeNumbers(int valores[]) {
        Arrays.stream(valores)
                .filter(this::isPrime)
                .forEach(System.out::println);
    }

}
