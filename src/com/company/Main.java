package com.company;

import java.util.Arrays;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
        HashSet<Integer> a = new HashSet<>();
        a.add(2);
        a.add(4);

        Exercices exercices = new Exercices();

        System.out.println("Factorial de 5: " + exercices.factorial(5));
        System.out.println("Sequencia 1 a 5 de n^2: " );
        exercices.printSequenceN2(5);
        System.out.println("Imprimir secuencia de numeros 0 a 10");
        exercices.printSequence(0, 10);

        System.out.println("Es primo el 5?");
        System.out.println(exercices.isPrime(5) ? "Si" : "No");
        System.out.println("Es primo el 8?");
        System.out.println(exercices.isPrime(8) ? "Si" : "No");

        int valores[] = { 3, 8, 10, 12, 7 };
        System.out.println("Números primos del arreglo " + Arrays.toString(valores));
        exercices.printPrimeNumbers(valores);

    }
}
